
import './App.css';

import UseState from './components/UseState/UseState'
import Ex2UseState from './components/UseState/Ex2'
import StateTutorial from './components/UseState/StateTutEx3'
import StateTutEx4 from './components/UseState/StateTutEx4'
import StateExample5 from './components/UseState/StateTutEx5'

import UseEffect from './components/UseEffect/UseEffect'
import EffectTutExample3 from './components/UseEffect/EffectEx3'

import UseReducerExample from './components/UseState/UseReducer/ReducerTut'

function App() {
  return (
    <div className="App">
      {/* <UseReducerExample /> */}
      {/* <StateExample5 /> */}
      {/* <StateTutorial />
      <StateTutEx4 /> */}
      {/* <UseState />
      <UseEffect />
      <Ex2UseState /> */}

      <EffectTutExample3 />
    </div>
  );
}

export default App;
