import React from 'react'

class Example extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }

    componentDidMount() {
        document.title = `You clicked ${this.state.count} times`
    }

    componentDidUpdate() {
        document.title = `You clicked ${this.state.count} times`
    }

    render() {
        return (
            <div>
                <p>You clicked {this.state.count} times.</p>
                <button onClick={() => this.setState({ count: this.state.count + 1})}>
                    Click Me!!
                </button>
            </div>
        )
    }
}

export default Example


//VS


// import React, { useState, useEffect } from 'react'

// const Example = () => {
//     const [count, setCount] = useState(0)

//     //Similar to componentDidMount and componentDidUpdate:
//     useEffect(() => {
//         //Update the document title using the browser API

//         document.title = `You clicked ${count} times`
//     })

//     return (
//         <div>
//            <p>You clicked {count} times</p>
//            <button onClick={() => setCount(count + 1)}>
//                Click Me
//            </button> 
//         </div>
//     )
// }

// export default Example