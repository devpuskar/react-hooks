import React, { useState, useEffect } from 'react'

const Example = () => {
    const [count, setCount] = useState(0)

    //Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        //Update the document title using the browser API

        document.title = `You clicked ${count} times`
    })

    return (
        <div>
           <p>You clicked {count} times</p>
           <button onClick={() => setCount(count + 1)}>
               Click Me
           </button> 
        </div>
    )
}

export default Example


// function FriendStatus(props) {
// 	const [isOnline, setIsOnline] = useState(null)
	
// 	function handleStatusChange(status){
// 		setIsOnline(status.isOnline)
// }


// useEffect(() => {
// ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange)
// return () => {
// ChatAPI.unsubscribeFromFriendsStatus(props.friend.id, handleStatusChange)
// }
// })

// if (isOnline === null) {
// 	return 'Loading…';
// }
// return isOnline ? 'Online' : 'Offline'
// }
