// PedroTech YC Example

import React, { useState } from 'react'

const StateTutorial = () => {
    // let counter = 0
    const [counter, setCounter] = useState(0)

    const increment = () => {
        // counter = counter + 1
        // console.log(counter)

        // setCounter(3)

        setCounter(counter + 1)
    }
    return (
        <div>
            {counter}
            <button onClick={increment}>
                Increment
            </button>
        </div>
    )
}

export default StateTutorial