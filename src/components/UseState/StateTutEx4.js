// PedroTech YC Example

import React, { useState } from 'react'

const StateTut = () => {
    const [inputValue, setInputValue] = useState('Puskar')

    let onChange = (event) => {
        const newValue = event.target.value
        setInputValue(newValue)
    }

    return (
        <div>
            <input 
                placeholder='enter something'
                onChange={onChange}
            />
            {inputValue}
        </div>
    )
}

export default StateTut
