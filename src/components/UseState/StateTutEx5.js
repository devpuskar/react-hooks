// PedroTech YC Example

// Checkout ReduceTut.js to use useReducer Hook instead of useState

import React, { useState } from 'react'

const MultiStateTut = () => {
    const [count, setCount] = useState(0)
    const [showText, setShowText] = useState(true)


    return (
        <div>
            <h1>{count}</h1>
            <button 
                onClick={() => {
                    setCount(count + 1)
                    setShowText(!showText)
                }}
            >
                Click Me!!
            </button>

            { showText && <p>This is a text</p> }
        </div>
    )
}

export default MultiStateTut
