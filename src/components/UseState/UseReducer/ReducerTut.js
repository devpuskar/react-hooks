// PedroTech YC Example

// Checkout StateTutEx5.js to use useState Hook instead of useState

import React, { useReducer } from 'react'

const reducer = (state, action) => {
    switch (action.type) {
        case "INCREMENT":
            return {count: state.count + 1, showText: state.showText}
        case "toggleShowText":
            return {count: state.count, showText: !state.showText}
        default: 
            return state
    }
}

const ReducerTut = () => {
    const [state, dispatch] = useReducer(reducer, { count: 0, showText: true})
    return (
        <div>
            <p>{state.count}</p>
            <button
                onClick={() => {
                    dispatch({ type: "INCREMENT" })
                    dispatch({ type: "toggleShowText" })
                }}
            >
                Click Here!
            </button>
            {state.showText && <p>This is a text</p>}
        </div>
    )
}

export default ReducerTut
